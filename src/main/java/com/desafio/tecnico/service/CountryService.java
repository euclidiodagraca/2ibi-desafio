package com.desafio.tecnico.service;

import com.desafio.tecnico.Utils.Constants;
import com.desafio.tecnico.models.Country;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class CountryService {

    public List<Country> allCountries() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Collections.singletonList(new MediaType("application", "json")));
        HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
        RestTemplate template = new RestTemplate();

        template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ResponseEntity<Country[]> responseEntity =
                template.exchange(Constants.BASE_URL + "all", HttpMethod.GET, requestEntity, Country[].class);

        return Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
    }

}
