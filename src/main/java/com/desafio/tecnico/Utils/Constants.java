package com.desafio.tecnico.Utils;

public class Constants {
    public static final String BASE_URL = "https://restcountries.eu/rest/v2/";
    public static final String CSV_FILE = "countries-csv.csv";
    public static final String EXCEL_FILE = "countries-excel.xls";
}
