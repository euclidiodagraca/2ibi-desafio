package com.desafio.tecnico.Utils;

import com.desafio.tecnico.models.Country;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.http.HttpHeaders;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class BuildCsvResponse {

    public static void writeCsv(HttpServletResponse response, List<Country> countries) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + Constants.CSV_FILE + "\"");

        try {
            var writer = new StatefulBeanToCsvBuilder<Country>(response.getWriter())
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                    .withOrderedResults(true)
                    .build();
            writer.write(countries);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
