package com.desafio.tecnico.Utils;

import com.desafio.tecnico.models.Country;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.view.document.AbstractXlsView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class BuildExcelResponse extends AbstractXlsView {
    @Override
    protected void buildExcelDocument(Map<String, Object> map, Workbook workbook, HttpServletRequest httpServletRequest,
                                      HttpServletResponse httpServletResponse) {

        httpServletResponse.setContentType("text/xls");
        httpServletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + Constants.EXCEL_FILE + "\"");

        List<Country> countries = (List<Country>) map.get("countries");
        Sheet sheet = workbook.createSheet("Lista de países");
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Nome");
        header.createCell(1).setCellValue("Capital");
        header.createCell(2).setCellValue("Região");
        header.createCell(3).setCellValue("Sub Região");
        header.createCell(4).setCellValue("População");
        header.createCell(5).setCellValue("Área");
        header.createCell(6).setCellValue("Fuso horário");
        header.createCell(7).setCellValue("Nome nativo");
        header.createCell(8).setCellValue("Código");
        header.createCell(9).setCellValue("Link da Bandeira");

        int rowNumber = 1;
        for (var country : countries) {
            Row row = sheet.createRow(rowNumber++);
            row.createCell(0).setCellValue(country.getName());
            row.createCell(1).setCellValue(country.getCapital());
            row.createCell(2).setCellValue(country.getRegion());
            row.createCell(3).setCellValue(country.getSubregion());
            row.createCell(4).setCellValue(country.getPopulation());
            row.createCell(5).setCellValue(country.getArea());
            row.createCell(6).setCellValue(country.getTimezones().toString());
            row.createCell(7).setCellValue(country.getNativeName());
            row.createCell(8).setCellValue(country.getNumericCode());
            row.createCell(9).setCellValue(country.getFlag());
        }

    }
}
