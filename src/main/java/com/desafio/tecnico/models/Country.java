package com.desafio.tecnico.models;

import com.opencsv.bean.CsvBindByName;
import java.util.ArrayList;

public class Country {
    @CsvBindByName(column = "Nome")
    private String name;
    @CsvBindByName(column = "Capital")
    private String capital;
    @CsvBindByName(column = "Região")
    private String region;
    @CsvBindByName(column = "Sub região")
    private String subregion;
    @CsvBindByName(column = "População")
    private float population;
    @CsvBindByName(column = "Área")
    private float area;
    @CsvBindByName(column = "Fuso horário")
    private ArrayList <String> timezones;
    @CsvBindByName(column = "Nome nativo")
    private String nativeName;
    @CsvBindByName(column = "Código")
    private String numericCode;
    @CsvBindByName(column = "Bandeira")
    private String flag;

    public Country() {
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public String getRegion() {
        return region;
    }

    public String getSubregion() {
        return subregion;
    }

    public float getPopulation() {
        return population;
    }

    public float getArea() {
        return area;
    }

    public ArrayList<String> getTimezones() {
        return timezones;
    }

    public String getNativeName() {
        return nativeName;
    }

    public String getNumericCode() {
        return numericCode;
    }

    public String getFlag() {
        return flag;
    }

}