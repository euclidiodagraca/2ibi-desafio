package com.desafio.tecnico.controllers;

import com.desafio.tecnico.service.CountryService;
import com.desafio.tecnico.Utils.BuildCsvResponse;
import com.desafio.tecnico.Utils.BuildExcelResponse;
import com.desafio.tecnico.models.Country;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping()
public class CountryController {

    @Autowired
    private CountryService service;

    @GetMapping(value = "client/countries/", produces= MediaType.APPLICATION_JSON_VALUE)
    private List<Country> countries() {
        return service.allCountries();
    }

    @RequestMapping("/")
    private ModelAndView homeView(){
        var modelAndView = new ModelAndView();
        modelAndView.setViewName("home");
        return modelAndView;
    }

    @GetMapping("client/countries/excel")
    public ModelAndView countriesToExcel(){
        List<Country> countries = service.allCountries();
        return new ModelAndView(new BuildExcelResponse(), "countries", countries);
    }

    @RequestMapping(value = "client/countries/csv", produces = "text/csv")
    public void countriesToCsv(HttpServletResponse response) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        BuildCsvResponse.writeCsv(response, service.allCountries());
    }

    @GetMapping(value="client/countries/xml", produces= MediaType.APPLICATION_XML_VALUE)
    public List<Country> countriesToXml() {
        return service.allCountries();
    }
}
